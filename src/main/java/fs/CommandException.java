package fs;

public class CommandException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CommandException(String cuase) {
		super(cuase);
	}
	
	public CommandException(Throwable cuase) {
		super(cuase);
	}

}
