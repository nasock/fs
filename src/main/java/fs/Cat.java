package fs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Cat implements CommandInterface {

	public String execute(ArrayList<String> strs) throws CommandException {
		if (strs.size() != 1) {
			throw new CommandException("wrong input");
		}
		String pathStr = strs.get(0);
		Path path = Paths.get(pathStr);
		File file = path.toFile();
		if (!file.exists()){
			throw new CommandException("wrong dir name");
		}
		try {
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(file));
				String line = null;
				StringBuilder strb = new StringBuilder();
				String ls = System.getProperty("line.separator");
				while ((line = reader.readLine()) != null) {
					strb.append(line);
					strb.append(ls);
				}
				return strb.toString();
			} finally {
				reader.close();
			}
		} catch (FileNotFoundException e) {
			throw new CommandException(e);
		} catch (IOException e) {
			throw new CommandException(e);
		}
	}

}
