package fs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Mv implements CommandInterface {

	private void checkPaths(Path pathSource, Path pathDest) throws CommandException {
		Path destParent = pathDest.getParent();
		if (destParent.equals(pathSource)) {
			throw new CommandException("wrong input");
		}
		File fileDestParent = destParent.toFile();
		if (!fileDestParent.exists()) {
			throw new CommandException("wrong input");
		}
		File fileSource = pathSource.toFile();
		File fileDest = pathDest.toFile();
		if (!fileSource.exists() || fileDest.exists()) {
			throw new CommandException("wrong input");
		}
	}

	private void copyDir(File fileSource, File fileDest) throws CommandException {
		if (!fileSource.isDirectory()) {
			copyFile(fileSource, fileDest);
		} else {
			fileDest.mkdirs();
			String[] children = fileSource.list();
			for (String childName : children) {
				Path childPathSource = fileSource.toPath().resolve(childName);
				Path childPathDest = fileDest.toPath().resolve(childName);
				File childSource = childPathSource.toFile();
				File childDest = childPathDest.toFile();
				copyDir(childSource, childDest);
			}
		}
	}

	@SuppressWarnings("resource")
	private void copyFile(File fileSource, File fileDest) throws CommandException {
		try {
			FileChannel sourceChannel = null;
			FileChannel destChannel = null;
			try {
				sourceChannel = new FileInputStream(fileSource).getChannel();
				destChannel = new FileOutputStream(fileDest).getChannel();
				destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
			} finally {
				sourceChannel.close();
				destChannel.close();
			}
		} catch (FileNotFoundException e) {
			throw new CommandException(e);
		} catch (IOException e) {
			throw new CommandException(e);
		}	}

	private void del(File file) {
		if (!file.isDirectory()) {
			file.delete();
		} else {
			String[] children = file.list();
			for (String childName : children) {
				Path childPath = file.toPath().resolve(childName);
				File child = childPath.toFile();
				del(child);
			}
			file.delete();
		}
	}
	
	private void move(Path pathSource, Path pathDest) throws CommandException {
		File fileSource = pathSource.toFile();
		File fileDest = pathDest.toFile();
		boolean rename = fileSource.renameTo(fileDest);
		if (!rename) {
			copyDir(fileSource, fileDest);
			del(fileSource);
		}
	}

	public String execute(ArrayList<String> line) throws CommandException {
		if (line.size() != 2) {
			throw new CommandException("wrong input");
		}
		String pathSourceStr = line.get(0);
		String pathDestStr = line.get(1);
		Path pathSource = Paths.get(pathSourceStr);
		Path pathDest = Paths.get(pathDestStr);
		checkPaths(pathSource, pathDest);
		move(pathSource, pathDest);

		return pathSourceStr + " was moved to " + pathDestStr;
	}

}
