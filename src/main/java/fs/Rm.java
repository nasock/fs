package fs;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Rm implements CommandInterface {

	private ArrayList<String> orderCommands(ArrayList<String> commands) {
		ArrayList<String> orderedCommands = new ArrayList<String>();
		for (int i = 0; i < commands.size(); i++) {
			String str = commands.get(i);
			if (str.equals("-f") || str.equals("-fr")) {
				orderedCommands.add(1, str);
			} else if (str.equals("-r")) {
				orderedCommands.add(2, str);
			} else {
				orderedCommands.add(0, str);
			}
		}
		return orderedCommands;
	}

	private String getCommand(ArrayList<String> commands, int ind) {
		if (ind < 0 || ind >= commands.size()) {
			return "";
		}
		String str = commands.get(ind);
		if (str == null) {
			str = "";
		}
		return str;
	}

	private void removeSimple(Path path, boolean force) throws CommandException {
		File file = path.toFile();
		if (!file.exists()) {
			throw new CommandException("wrong input");
		}
		if (!file.isDirectory()) {
			file.delete();
		} else {
			String[] children = file.list();
			if (children.length == 0 && force) {
				file.delete();
			} else {
				throw new CommandException("wrong input");
			}
		}
	}

	private void removeRecursive(Path path, boolean force) throws CommandException {
		File file = path.toFile();
		if (!file.exists()) {
			throw new CommandException("wrong input");
		}

		if (!file.isDirectory()) {
			file.delete();
		} else {
			String[] children = file.list();
			for (String childName : children) {
				Path childPath = path.resolve(childName);
				removeRecursive(childPath, force);
			}
			if (force) {
				file.delete();
			} else {
				throw new CommandException("wrong input");
			}
		}
	}

	public String execute(ArrayList<String> commands) throws CommandException {
		ArrayList<String> orderedCommands = orderCommands(commands);
		String pathStr = getCommand(orderedCommands, 0);
		String command1 = getCommand(orderedCommands, 1);
		String command2 = getCommand(orderedCommands, 2);

		Path path = Paths.get(pathStr);
		boolean force = false;
		if (command1.equals("-f") || command1.equals("-fr")) {
			force = true;
		}

		if (command2.equals("-r") || command1.equals("-fr")) {
			removeRecursive(path, force);
		} else {
			removeSimple(path, force);
		}

		return pathStr + " was deleted";
	}

}
