package fs;

import java.util.ArrayList;

public interface CommandInterface {
	public String execute(ArrayList<String> line) throws CommandException;
}
