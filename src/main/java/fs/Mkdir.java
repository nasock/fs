package fs;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Mkdir implements CommandInterface {

	public String execute(ArrayList<String> dirPaths) throws CommandException {
		if (dirPaths.size() < 1) {
			throw new CommandException("wrong input");
		}
		StringBuilder strb = new StringBuilder();
		String ls = System.getProperty("line.separator");
		for (String pathStr : dirPaths) {
			Path path = Paths.get(pathStr);
			File file = path.toFile();
			boolean b = file.mkdirs();
			strb.append(pathStr + " ");
			if (b) {
				strb.append("was created" + ls);
			} else {
				strb.append("wasn't created" + ls);
			}
		}
		return strb.toString();
	}
}
