package fs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Touch implements CommandInterface {

	public String execute(ArrayList<String> strs) throws CommandException {
		if (strs.size() != 1) {
			throw new CommandException("wrong input");
		}
		String pathStr = strs.get(0);
		Path path = Paths.get(pathStr);
		File file = path.toFile();
		long timestamp = System.currentTimeMillis();

		try {
			if (!file.exists()) {
				new FileOutputStream(file).close();
			}
		} catch (IOException e) {
			throw new CommandException(e);
		}
		file.setLastModified(timestamp);
		return timestamp + "";
	}

}
