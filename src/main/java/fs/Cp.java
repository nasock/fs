package fs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Cp implements CommandInterface {
	private ArrayList<String> orderCommands(ArrayList<String> commands) {
		ArrayList<String> orderedCommands = new ArrayList<String>();
		for (int i = 0; i < commands.size(); i++) {
			String str = commands.get(i);
			if (str.equals("-r")) {
				orderedCommands.add(2, str);
			} else {
				orderedCommands.add(str);
			}
		}
		return orderedCommands;
	}

	private String getCommand(ArrayList<String> commands, int ind) {
		if (ind < 0 || ind >= commands.size()) {
			return "";
		}
		String str = commands.get(ind);
		if (str == null) {
			str = "";
		}
		return str;
	}

	private void copyRecursive(Path pathSource, Path pathDest) throws CommandException {
		if (pathSource.equals(pathDest)) {
			throw new CommandException("wrong input");
		}
		File fileSource = pathSource.toFile();
		File fileDest = pathDest.toFile();
		if (!fileSource.exists() || fileDest.exists()) {
			throw new CommandException("wrong input");
		}

		if (!fileSource.isDirectory()) {
			copySimple(pathSource, pathDest);
		} else {
			fileDest.mkdirs();
			String[] children = fileSource.list();
			for (String childName : children) {
				Path childPathSource = pathSource.resolve(childName);
				Path childPathDest = pathDest.resolve(childName);
				copyRecursive(childPathSource, childPathDest);
			}
		}
	}

	@SuppressWarnings("resource")
	private void copySimple(Path pathSource, Path pathDest) throws CommandException {
		File fileSource = pathSource.toFile();
		File fileDest = pathDest.toFile();
		if (!fileSource.exists() || fileDest.exists()) {
			throw new CommandException("wrong input");
		} else if (fileSource.isDirectory()) {
			throw new CommandException("wrong input");
		}

		try {
			FileChannel sourceChannel = null;
			FileChannel destChannel = null;
			try {
				sourceChannel = new FileInputStream(fileSource).getChannel();
				destChannel = new FileOutputStream(fileDest).getChannel();
				destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
			} finally {
				sourceChannel.close();
				destChannel.close();
			}
		} catch (FileNotFoundException e) {
			throw new CommandException(e);
		} catch (IOException e) {
			throw new CommandException(e);
		}
	}

	public String execute(ArrayList<String> commands) throws CommandException {
		ArrayList<String> orderedCommands = orderCommands(commands);
		String pathSourceStr = getCommand(orderedCommands, 0);
		String pathDestStr = getCommand(orderedCommands, 1);
		String commandR = getCommand(orderedCommands, 2);

		Path pathSource = Paths.get(pathSourceStr);
		Path pathDest = Paths.get(pathDestStr);

		if (commandR.equals("-r")) {
			copyRecursive(pathSource, pathDest);
		} else {
			copySimple(pathSource, pathDest);
		}
		return pathSourceStr + "copied to " + pathDestStr;
	}

}
