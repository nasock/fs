package fs;

import java.util.ArrayList;
import java.util.HashMap;

public class App {
	private HashMap<String, CommandInterface> commands;

	public App() {
		commands = new HashMap<String, CommandInterface>();
		commands.put("ls", new Ls());
		commands.put("mkdir", new Mkdir());
		commands.put("cat", new Cat());
		commands.put("touch", new Touch());
		commands.put("cp", new Cp());
		commands.put("rm", new Rm());
		commands.put("mv", new Mv());
		commands.put("mktree", new Mktree());
	}

	private CommandInterface getCommand(String str) {
		return commands.get(str);
	}

	private ArrayList<String> cleanLine(String[] args) {
		ArrayList<String> strings = new ArrayList<String>();
		for (int i = 1; i < args.length; i++) {
			String str = args[i];
			str = str.trim();
			if (!str.isEmpty()) {
				strings.add(str);
			}
		}
		return strings;
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("no input");
			return;
		}

		String commandStr = args[0];
		App app = new App();
		CommandInterface command = app.getCommand(commandStr);
		if(command == null) {
			System.out.println("wrong command");
			return;
		}
		ArrayList<String> line = app.cleanLine(args);
		try {
			String str = command.execute(line);
			System.out.println(str);
		} catch (CommandException e) {
			e.printStackTrace();
		}
		
	}
}
