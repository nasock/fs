package fs;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Ls implements CommandInterface {
	private void buildRecursive(Path path, StringBuilder strb, int level, boolean allFiles) throws CommandException {
		for (int i = 0; i < level - 1; i++) {
			strb.append("  ");
		}
		File file = path.toFile();
		if (!file.exists()) {
			throw new CommandException("wrong input");
		}
		String ls = System.getProperty("line.separator");
		if (level != 0) {
			strb.append(file.getName());
			if (!file.isDirectory()) {
				strb.append(ls);
				return;
			}
			strb.append("/" + ls);
		}

		String[] children = file.list();
		for (String childName : children) {
			if ((!allFiles) && childName.substring(0, 1).equals(".")) {
				continue;
			}
			Path childPath = path.resolve(childName);
			buildRecursive(childPath, strb, level + 1, allFiles);
		}
	}

	private void buildSimple(Path path, StringBuilder strb, boolean allFiles) throws CommandException {
		File file = path.toFile();
		if (!file.exists()) {
			throw new CommandException("wrong input");
		}
		String[] children = file.list();

		for (String childName : children) {
			if ((!allFiles) && childName.substring(0, 1).equals(".")) {
				continue;
			}
			strb.append(childName);
			Path childPath = path.resolve(childName);
			file = childPath.toFile();
			if (file.isDirectory()) {
				strb.append("/");
			}
			strb.append("  ");
		}
	}

	private String getCommand(ArrayList<String> commands, int ind) {
		if (ind < 0 || ind >= commands.size()) {
			return "";
		}
		String str = commands.get(ind);
		if (str == null) {
			str = "";
		}
		return str;
	}

	private ArrayList<String> orderCommands(ArrayList<String> commands) {
		ArrayList<String> orderedCommands = new ArrayList<String>();
		for (int i = 0; i < commands.size(); i++) {
			String str = commands.get(i);
			if (str.equals("-a") || str.equals("-ar")) {
				orderedCommands.add(1, str);
			} else if (str.equals("-r")) {
				orderedCommands.add(2, str);
			} else {
				orderedCommands.add(0, str);
			}
		}
		return orderedCommands;
	}

	public String execute(ArrayList<String> commands) throws CommandException {
		StringBuilder strb = new StringBuilder();
		ArrayList<String> orderedCommands = orderCommands(commands);
		String pathStr = getCommand(orderedCommands, 0);
		String command1 = getCommand(orderedCommands, 1);
		String command2 = getCommand(orderedCommands, 2);
		
		Path path = Paths.get(pathStr);
		boolean allFiles = false;
		if (command1.equals("-a") || command1.equals("-ar")) {
			allFiles = true;
		}

		if (command2.equals("-r") || command1.equals("-ar")) {
			buildRecursive(path, strb, 0, allFiles);
		} else {
			buildSimple(path, strb, allFiles);
		}
		return strb.toString();
	}
}
