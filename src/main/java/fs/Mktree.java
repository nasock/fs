package fs;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Mktree implements CommandInterface {

	private void checkPaths(Path pathSource, Path pathDest) throws CommandException {
		if (pathSource.equals(pathDest)) {
			throw new CommandException("wrong input");
		}
		File fileSource = pathSource.toFile();
		File fileDest = pathDest.toFile();
		if (!fileSource.exists() || !fileDest.exists()) {
			throw new CommandException("wrong input");
		}
		if(!fileSource.isDirectory() || !fileDest.isDirectory() ) {
			throw new CommandException("wrong input");
		}
	}
	
	private void makeTree(Path pathSource, Path pathDest) throws CommandException {
		File fileSource = pathSource.toFile();
		if(!fileSource.isDirectory()) {
			return;
		}
		
		Path newPath = pathDest.resolve(pathSource.getFileName());
		File newFile = newPath.toFile();
		boolean b = newFile.mkdirs();
		if (!b) {
			throw new CommandException("dir wasn't created");
		}
		
		String[] children = fileSource.list();
		for (String childName : children) {
			Path childSourcePath = pathSource.resolve(childName);
			makeTree(childSourcePath, newPath);
		}
	}
	
	public String execute(ArrayList<String> line) throws CommandException {
		if (line.size() != 2) {
			throw new CommandException("wrong input");
		}
		String pathSourceStr = line.get(0);
		String pathDestStr = line.get(1);
		Path pathSource = Paths.get(pathSourceStr);
		Path pathDest = Paths.get(pathDestStr);
		checkPaths(pathSource, pathDest);
		makeTree(pathSource, pathDest);
		
		return null;
	}

}
